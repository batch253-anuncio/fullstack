console.log(document.querySelector("#txt-first-name"));
/*
	Alternative ways to access HTML elements. This is what we can use aside from the querySelector().

	document.getElementById("txt-first-name");
	document.getElementsByClassName("txt-first-name");
	document.getElementsByTagName("input");
*/
console.log(document.getElementById("txt-first-name"));

const firstName = document.querySelector("#txt-first-name");
const lastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

console.log(firstName);
console.log(spanFullName);

/*

	Event
		ex: clock, hover, keypress and many other events

	Event Listeners
		Allows us to let our user/s intereact with our page. With eaach clck or hover there is an event which triggers a function/task.

	Syntax:
		selectedElement.addEventListener("event", function);

*/
firstName.addEventListener("keyup", (event) => {
  spanFullName.innerHTML = firstName.value;
});

lastName.addEventListener("onClick", printLastName);

function printLastName(event) {
  spanFullName.innerHTML = lastName.value;
}

const labelfirstName = document.querySelector("#label-first-name");

labelfirstName.addEventListener("click", (e) => {
  alert("you clicked me ");
});

/*
	The "event" argument contains the information on the triggered event.
	The "event.target" contains the element where the event happened.
	the "event.target.value" gets the value of the input object (this is similar to txtFirstName.value).
*/
