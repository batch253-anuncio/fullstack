const firstName = document.querySelector("#txt-first-name");
const lastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

console.log(firstName);
console.log(spanFullName);

firstName.addEventListener("keyup", concatFullName);
lastName.addEventListener("keyup", concatFullName);

function concatFullName(event) {
  spanFullName.innerHTML = firstName.value + " " + lastName.value;
}
